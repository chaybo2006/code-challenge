import {HTTP} from 'meteor/http';

export const Authentication = {
    login: (params, callBack) => {
        HTTP.post('/api/login', {
            data: params
        }, function (err, response) {
            let res = response.data;
            if (err) {
                Authentication.logOut();
            }else {
                localStorage.setItem('token', res.data.authToken);
                localStorage.setItem('userId', res.data.userId);
            }
            callBack(res);
        });
    },
    isLogin: () => {
        let token = localStorage.getItem('token');
        let userId = localStorage.getItem('userId');
        return !!(token && userId);
    },
    logOut: (callBack) => {
        HTTP.get('/api/logout', {
            headers: {
                'X-Auth-Token': Authentication.getToken(),
                'X-User-Id': Authentication.getUserId()
            }
        }, function (err) {
            if (!err) {
                localStorage.removeItem('token');
                localStorage.removeItem('userId');
            }
            callBack(!Authentication.isLogin());
        })
    },
    getToken: () => {
        return localStorage.getItem('token');
    },
    getUserId: () => {
        return localStorage.getItem('userId');
    }
}
