import {HTTP} from "meteor/http";
import {Authentication} from "./authentical";
import {Meteor} from "meteor/meteor";
import {message} from 'antd'

class Api {
    defaultHeader = {};
    constructor() {
        if (Authentication.isLogin()) {
            this.defaultHeader = {
                'X-Auth-Token': Authentication.getToken(),
                'X-User-Id': Authentication.getUserId()
            };
        }
    }

    call = (method, url, callBack, options = {}) => {
        options.headers = Object.assign(options.headers || {}, this.defaultHeader);
        HTTP.call(method, url, options, (err, res) => {
            if (err) {
                message.error(res.data.message);
                callBack(res.data);
                throw new Meteor.Error(res.statusCode, res.data.message);
            }
            callBack(res.data);
        });
    }
}

export default Api;
