import React from 'react';
import {Button, Layout, Menu, Result} from "antd";
import 'antd/dist/antd.css';
import {Link, Route, Switch, useHistory} from 'react-router-dom';
import {Authentication} from "../api/authentical";
import {Login} from "./Page/Login/login";
import {Home} from "./Page/Home/home";
import {Users} from "./Page/Users";
import {UserForm} from "./Page/Users/form";
import {UserEdit} from "./Page/Users/edit";
import {UserCreate} from "./Page/Users/create";
import {Department} from "./Page/Department";
import {DepartmentCreate} from "./Page/Department/create";
import {DepartmentEdit} from "./Page/Department/edit";
import {Team} from "./Page/Team";
import {TeamCreate} from "./Page/Team/create";
import {TeamEdit} from "./Page/Team/edit";

const {Content, Sider, Footer} = Layout;

export const App = () => {
    let history = useHistory();

    return <Layout style={{minHeight: '100vh'}}>
        {Authentication.isLogin() ? <Sider className="site-layout-background">
            <Menu>
                <Menu.Item>
                    <Link to="/users">Users</Link>
                </Menu.Item>
                <Menu.Item>
                    <Link to="/department">Department</Link>
                </Menu.Item>
                <Menu.Item>
                    <Link to="/team">Team</Link>
                </Menu.Item>
                <Menu.Item onClick={() => {
                    Authentication.logOut((bool) => {
                        if (bool)
                            window.location.href = '/';
                    });
                }}>
                    Logout
                </Menu.Item>
            </Menu>
        </Sider> : null}
        <Layout style={{padding: '0 24px'}}>
            <Content
                className="site-layout-background"
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                }}
            >
                <Switch>
                    {!Authentication.isLogin() ? <Route exact={true} path="/"><Login/></Route> : <React.Fragment>
                        <Route exact={true} path={['/', "/home"]}><Home/></Route>
                        <Route exact={true} path="/users"><Users/></Route>
                        <Route path={"/users/edit/:id"}><UserEdit/></Route>
                        <Route path={"/users/create"}><UserCreate/></Route>
                        <Route exact={true} path={"/department"}><Department/></Route>
                        <Route path={"/department/create"}><DepartmentCreate/></Route>
                        <Route path={"/department/edit/:id"}><DepartmentEdit/></Route>
                        <Route exact={true} path={"/team"}><Team/></Route>
                        <Route path={"/team/create"}><TeamCreate/></Route>
                        <Route path={"/team/edit/:id"}><TeamEdit/></Route>
                    </React.Fragment>}
                    <Route exact>
                        <Result
                            status="404"
                            title="404"
                            subTitle="Sorry, the page you visited does not exist."
                            extra={<Button onClick={() => history.push('/')} type="primary">Back Home</Button>}
                        />
                    </Route>
                </Switch>
            </Content>
            <Footer style={{textAlign: 'center'}}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
    </Layout>;
};
