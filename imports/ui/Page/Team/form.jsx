import React, {useState, useEffect} from 'react';
import {Button, Form, Input, Select, Transfer} from "antd";
import Api from "../../../api/http";

export const TeamForm = ({onFinish, team = {}, ...props}) => {
    const [departments, setDepartments] = useState([]);
    const [users, setUsers] = useState([]);
    const [usersInTeam, setUsersInTeam] = useState([]);
    const [form] = Form.useForm();
    const api = new Api();

    useEffect(() => {
        api.call('get', '/api/department', res => {
            if (res.status === 'success')
                setDepartments(res.data.map(depart => {
                    return {label: depart.name, value: depart._id}
                }));
        });
    }, []);

    useEffect(() => {
        form.resetFields();
        if (Object.keys(team).length > 0) {
            setUsersInTeam(team.staffs);
        }
        api.call('get', '/api/users?roles=staff', res => {
            if (res.status === 'success') {
                setUsers(res.data.map(user => (
                    {
                        key: user._id,
                        title: user.profile.fullName,
                        email: user.profile.email
                    }
                )));
            }
        });
    }, [team._id]);

    const layout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 14 },
    };

    const validateMessages = {
        required: '${label} is required!'
    }

    return (
        <Form form={form} {...layout} onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item initialValue={team.name} name={['team', 'name']} label="Name" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item initialValue={team.department} name={['team', 'department']} label="Department" rules={[{ required: true }]}>
                <Select options={departments}/>
            </Form.Item>
            <Form.Item name={['team', 'staffs']} label="Staff">
                <Transfer
                    showSearch
                    listStyle={{width: 300}}
                    pagination
                    targetKeys={usersInTeam}
                    dataSource={users}
                    titles={['Staffs', 'Team Staffs']}
                    render={item => item.title}
                    onChange={(targetKeys) => {
                        form.setFieldsValue({
                            staffs: targetKeys
                        });
                        setUsersInTeam(targetKeys);
                    }}
                />
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 4 }}>
                <Button loading={props.loading} type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
