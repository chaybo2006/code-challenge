import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import Api from "../../../api/http";
import {message, PageHeader} from "antd";
import {TeamForm} from "./form";

export const TeamCreate = () => {
    const history = useHistory();
    const api = new Api();
    const [onSubmit, setOnSubmit] = useState(false);

    const onFinish = values => {
        setOnSubmit(true);
        api.call('post', '/api/team', res => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success('Create team '+res.data.name+' success.');
                history.push('/team');
            }
        }, {
            data: values.team
        })
    }

    return (
        <PageHeader
            title={"Team create"}
        >
            <TeamForm onFinish={onFinish} loading={onSubmit} />
        </PageHeader>
    );
}
