import React, {useState, useEffect} from 'react';
import {Button, message, PageHeader, Select, Space, Table} from "antd";
import {Link} from "react-router-dom";
import Api from "../../../api/http";
import moment from "moment";

export const Team = () => {
    const api = new Api();
    const [teams, setTeams] = useState([]);
    const [departments, setDepartments] = useState([]);
    const [selectDepart, setSelectDepart] = useState(null);

    function fetch() {
        api.call('get', '/api/team', res => {
            if (res.status === 'success')
                setTeams(res.data);
        });
        api.call('get', '/api/department', res => {
            if (res.status === 'success')
                setDepartments(res.data.map(item => ({text: item.name, value: item._id})));
        });
    }

    useEffect(() => {
        fetch();
    }, []);

    const columns = [
        {
            title: 'ID',
            dataIndex: '_id',
            key: 'id'
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Department',
            dataIndex: ['department', 'name'],
            key: 'department',
            filters: departments/*({setSelectedKeys, selectedKeys, confirm, clearFilters}) => {
                return <div style={{ padding: 8 }}>
                    <Select onChange={val => {
                        setSelectedKeys(val ? [val] : []);
                        confirm();
                        setSelectDepart(val);
                    }} value={selectDepart} options={departments} style={{minWidth: 200, marginBottom: 8, display: 'block'}}/>
                    <Space>
                        <Button onClick={() => {
                            clearFilters();
                            setSelectDepart(null);
                        }} size="small" style={{ width: 90 }}>
                            Reset
                        </Button>
                    </Space>
                </div>
            }*/,
            filterMultiple: false,
            onFilter: (value, record) => {
                return record.department._id === value;
            },
            render: (text, record, index) => {
                return text;
            }
        },
        {
            title: 'Created date',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: text => text ? moment(text).format('DD/MM/YYYY') : null
        },
        {
            title: 'Action',
            key: 'action',
            align: 'right',
            render: (text, record) => (
                <Space size="middle">
                    <Button type="link"><Link to={"/team/edit/"+record._id}>Edit</Link></Button>
                    <Button type="danger" onClick={() => api.call('delete', '/api/team/'+record._id, res => {
                        if (res.status === 'success') {
                            fetch();
                            message.success(res.data.message);
                        }
                    })}>Delete</Button>
                </Space>
            )
        }
    ];

    return <PageHeader
        title="Team manager"
        extra={[
            <Button type="primary"><Link to="/team/create">Create</Link></Button>
        ]}>
        <Table columns={columns} dataSource={teams}/>
    </PageHeader>
}
