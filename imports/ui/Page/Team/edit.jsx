import React, {useState, useEffect} from 'react';
import {message, PageHeader} from "antd";
import {TeamForm} from "./form";
import {useHistory, useParams} from "react-router-dom";
import Api from "../../../api/http";

export const TeamEdit = () => {
    const history = useHistory();
    const api = new Api();
    const [onSubmit, setOnSubmit] = useState(false);
    const {id} = useParams();
    const [team, setTeam] = useState();

    useEffect(() => {
        api.call('get', '/api/team/'+id, res => {
            if (res.status === 'success')
                setTeam(res.data);
        });
    }, [id])

    const onFinish = values => {
        setOnSubmit(true);
        api.call('put', '/api/team/'+id, res => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success('Edit department '+res.data.name+' success.');
                history.push('/team');
            }
        }, {
            data: values.team
        })
    }

    return <PageHeader
        title="Team edit"
    >
        <TeamForm onFinish={onFinish} loading={onSubmit} team={team} />
    </PageHeader>
}
