import React from 'react';
import {Button, Form, Input, Result, message} from "antd";
import './login.scss';
import {Authentication} from "../../../api/authentical";

export const Login = () => {

    const onFinish = values => {
        Authentication.login(values, (res) => {
            if (res.status === 'error') {
                message.error(res.message);
            }else {
                message.success('Login success.');
                location.reload();
            }
        });
    }

    return (
        <Result title="Login" className="login-page">
            <Form onFinish={onFinish} className="login-form">
                <Form.Item
                    name="username"
                    required={true}
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="password"
                    required={true}
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Result>
    );
}
