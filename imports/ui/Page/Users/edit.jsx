import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from "react-router-dom";
import Api from "../../../api/http";
import {message, PageHeader} from "antd";
import {UserForm} from "./form";

export const UserEdit = () => {
    const [onSubmit, setOnSubmit] = useState(false);
    const [user, setUser] = useState({});
    const {id} = useParams();
    const history = useHistory();
    const api = new Api();

    useEffect(() => {
        api.call('get', '/api/users/'+id, (res) => {
            if (res.status === 'success') {
                setUser(res.data);
            }
        });
    }, [id]);

    const onFinish = values => {
        setOnSubmit(true);
        delete values.user['confirm-password'];
        let profile = values.user.profile;
        delete values.user.profile;
        api.call('put', '/api/users/'+id, (res) => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success(id ? 'Edit users '+res.data.profile.fullName+' success.' : 'Create users '+res.data.profile.fullName+' success.');
                history.push('/users');
            }
        }, {
            data: {
                user: values.user,
                profile: profile
            }
        });
    }

    return (
        <PageHeader
            title={"User edit"}
        >
            <UserForm onFinish={onFinish} loading={onSubmit} user={user} />
        </PageHeader>
    );
}
