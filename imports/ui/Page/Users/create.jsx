import React, {useState} from 'react';
import {message, PageHeader} from "antd";
import {UserForm} from "./form";
import {Authentication} from "../../../api/authentical";
import Api from "../../../api/http";
import {useHistory} from "react-router-dom";

export const UserCreate = () => {
    const history = useHistory();
    const api = new Api();
    const [onSubmit, setOnSubmit] = useState(false);

    const onFinish = values => {
        setOnSubmit(true);
        delete values.user['confirm-password'];
        let profile = values.user.profile;
        delete values.user.profile;
        api.call('post', '/api/users', (res) => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success('Create users '+res.data.profile.fullName+' success.');
                history.push('/users');
            }
        }, {
            data: {
                user: values.user,
                profile: profile
            }
        });
    }

    return (
        <PageHeader
            title={"User create"}
        >
            <UserForm onFinish={onFinish} loading={onSubmit} />
        </PageHeader>
    );
}
