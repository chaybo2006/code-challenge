import React, {useState, useEffect} from 'react';
import {Button, message, PageHeader, Space, Table} from "antd";
import Api from "../../../api/http";
import {Link} from "react-router-dom";
import moment from "moment";

export const Users = () => {
    const [pageSize, setPageSize] = useState(100);
    const [currentPage, setCurrentPage] = useState(1);
    const [roles, setRoles] = useState(null);
    const [total, setTotal] = useState(0);

    const columns = [
        {
            title: 'ID',
            dataIndex: '_id',
            key: 'id',
            fixed: 'left'
        },
        {
            title: 'FullName',
            dataIndex: ['profile', 'fullName'],
            key: 'name'
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username'
        },
        {
            title: 'Position',
            dataIndex: ['profile', 'position'],
            key: 'position',
            filters: [
                {text: 'Manager', value: 'manager'},
                {text: 'Staff', value: 'staff'},
            ],
            filterMultiple: false,
            onFilter: (value, record) => {
                setRoles(value);
                return record.profile?.position === value;
            }
        },
        {
            title: 'Email',
            dataIndex: ['profile', 'email'],
            key: 'email'
        },
        {
            title: 'Birthday',
            dataIndex: ['profile', 'birthday'],
            key: 'birthday',
            render: text => {
                return text ? moment(text).format('DD/MM/YYYY') : null;
            }
        },
        {
            title: 'Action',
            key: 'action',
            align: 'right',
            fixed: 'right',
            render: (text, record) => (
                <Space size="middle">
                    <Button type="link"><Link to={"/users/edit/"+record._id}>Edit</Link></Button>
                    <Button type="danger" onClick={() => api.call('delete', 'api/users/'+record._id, res => {
                        if (res.status === 'success') {
                            fetch();
                            message.success(res.data.message);
                        }
                    })}>Delete</Button>
                </Space>
            )
        }
    ];
    const api = new Api();
    const [users, setUsers] = useState([]);

    function fetch() {
        api.call('get','api/users', (res) => {
            if (res.status === 'success') {
                setUsers(res.data);
                setTotal(res.count);
            }
        }, {
            params: {
                limit: pageSize,
                skip: currentPage,
                roles: roles
            }
        });
    }

    useEffect(() => {
        fetch();
    }, [pageSize, currentPage, roles]);

    return (
        <PageHeader
            title="Users manager"
            extra={[
                <Button type="primary"><Link to="/users/create">Create</Link></Button>
            ]}
        >
            <Table pagination={{
                defaultPageSize: pageSize,
                pageSize: pageSize,
                pageSizeOptions: [100, 500, 1000, 1500],
                total: total,
                onShowSizeChange: (cur, size) => {
                    setPageSize(size);
                    setCurrentPage(cur);
                },
                onChange: (pageNumber) => {
                    setCurrentPage(pageNumber);
                }
            }} scroll={{x: 1200}} columns={columns} dataSource={users}/>
        </PageHeader>
    );
}
