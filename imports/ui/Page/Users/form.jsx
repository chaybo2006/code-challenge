import React, {useEffect} from 'react';
import {Form, Input, DatePicker, Button, Select} from "antd";
import moment from 'moment';

export const UserForm = ({user = {}, onFinish, ...props}) => {
    const {Option} = Select;
    const [form] = Form.useForm();
    const layout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 14 },
    };

    useEffect(() => {
        form.resetFields();
    }, [user.profile])

    const validateMessages = {
        required: '${label} is required!',
        types: {
            email: '${label} is not a valid email!',
            number: '${label} is not a valid number!',
            date: '${label} is not a valid date'
        }
    }

    return (
        <Form form={form} {...layout} onFinish={onFinish} validateMessages={validateMessages}>
            <React.Fragment>
                <Form.Item initialValue={user.username} name={['user', 'username']} label="Username" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={['user', 'password']} label="Password">
                    <Input.Password />
                </Form.Item>
                <Form.Item name={['user', 'confirm-password']} label="Confirm Password" rules={[
                    ({ getFieldValue }) => ({
                        validator(_, value) {
                            if (getFieldValue(['user', 'password']) === value)
                                return Promise.resolve();
                            if (!value && !getFieldValue(['user', 'password']))
                                return Promise.resolve();
                            return Promise.reject('The passwords that you entered do not match!');
                        }
                    })
                ]}>
                    <Input.Password />
                </Form.Item>
            </React.Fragment>
            <Form.Item initialValue={user.profile?.fullName} name={['user', 'profile', 'fullName']} label="Full name" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item initialValue={user.profile?.email} name={['user', 'profile', 'email']} label="Email" rules={[{ required: true, type: 'email' }]}>
                <Input />
            </Form.Item>
            <Form.Item initialValue={user.profile?.birthday ? moment(user.profile?.birthday, 'YYYY/MM/DD') : null} name={['user', 'profile', 'birthday']} label="Birthday" rules={[{ type: 'date' }]}>
                <DatePicker format={'DD/MM/YYYY'} />
            </Form.Item>
            {user.roles?.includes('admin') ? null : <Form.Item initialValue={user.profile?.position} name={['user', 'profile', 'position']} label="Position" rules={[{ required: true }]}>
                <Select>
                    <Option value="manager">Manager</Option>
                    <Option value="staff">Staff</Option>
                </Select>
            </Form.Item>}
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 4 }}>
                <Button loading={props.loading} type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
