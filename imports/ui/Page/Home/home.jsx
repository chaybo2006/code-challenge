import React from 'react';
import {Result} from "antd";
import {SmileOutlined} from '@ant-design/icons';

export const Home = () => {

    return (
        <Result
            icon={<SmileOutlined />}
            title="Welcome"
        />
    );
}
