import React, {useState, useEffect} from 'react';
import {message, PageHeader} from "antd";
import {DepartmentForm} from "./form";
import {useHistory, useParams} from "react-router-dom";
import Api from "../../../api/http";

export const DepartmentEdit = () => {
    const history = useHistory();
    const api = new Api();
    const [onSubmit, setOnSubmit] = useState(false);
    const {id} = useParams();
    const [department, setDepartment] = useState();

    useEffect(() => {
        api.call('get', '/api/department/'+id, res => {
            if (res.status === 'success')
                setDepartment(res.data);
        });
    }, [id])

    const onFinish = values => {
        setOnSubmit(true);
        api.call('put', '/api/department/'+id, res => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success('Edit department '+res.data.name+' success.');
                history.push('/department');
            }
        }, {
            data: values.department
        })
    }

    return <PageHeader
        title="Department edit"
    >
        <DepartmentForm onFinish={onFinish} loading={onSubmit} department={department} />
    </PageHeader>
}
