import React, {useState, useEffect} from 'react';
import {Button, message, PageHeader, Space, Table} from "antd";
import {Link} from "react-router-dom";
import Api from "../../../api/http";
import moment from "moment";

export const Department = () => {
    const api = new Api();
    const [department, setDepartment] = useState([]);

    function fetch() {
        api.call('get', '/api/department', res => {
            if (res.status === 'success')
                setDepartment(res.data);
        });
    }

    useEffect(() => {
        fetch();
    }, []);

    const columns = [
        {
            title: 'ID',
            dataIndex: '_id',
            key: 'id'
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Manager',
            dataIndex: ['belongto', 'profile', 'fullName'],
            key: 'manager'
        },
        {
            title: 'Created date',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: text => text ? moment(text).format('DD/MM/YYYY') : null
        },
        {
            title: 'Action',
            key: 'action',
            align: 'right',
            render: (text, record) => (
                <Space size="middle">
                    <Button type="link"><Link to={"/department/edit/"+record._id}>Edit</Link></Button>
                    <Button type="danger" onClick={() => api.call('delete', '/api/department/'+record._id, res => {
                        if (res.status === 'success') {
                            fetch();
                            message.success(res.data.message);
                        }
                    })}>Delete</Button>
                </Space>
            )
        }
    ];

    return <PageHeader
        title="Departments manager"
        extra={[
            <Button type="primary"><Link to="/department/create">Create</Link></Button>
        ]}>
        <Table columns={columns} dataSource={department}/>
    </PageHeader>
}
