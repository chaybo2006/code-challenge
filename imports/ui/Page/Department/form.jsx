import React, {useState, useEffect} from 'react';
import {Button, Form, Input, Select} from "antd";
import Api from "../../../api/http";

export const DepartmentForm = ({onFinish, department = {}, ...props}) => {
    const [manager, setManager] = useState([]);
    const [form] = Form.useForm();
    const api = new Api();

    useEffect(() => {
        api.call('get', '/api/users?roles=manager', res => {
            if (res.status === 'success')
                setManager(res.data.map(user => {
                    return {label: user.profile.fullName, value: user._id}
                }));
        });
    }, [])

    useEffect(() => {
        form.resetFields();
    }, [department]);

    const layout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 14 },
    };

    const validateMessages = {
        required: '${label} is required!'
    }

    return (
        <Form form={form} {...layout} onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item initialValue={department.name} name={['department', 'name']} label="Name" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item initialValue={department.belongto} name={['department', 'belongto']} label="Manager" rules={[{ required: true }]}>
                <Select options={manager}/>
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 4 }}>
                <Button loading={props.loading} type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
