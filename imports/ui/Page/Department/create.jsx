import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import Api from "../../../api/http";
import {message, PageHeader} from "antd";
import {DepartmentForm} from "./form";

export const DepartmentCreate = () => {
    const history = useHistory();
    const api = new Api();
    const [onSubmit, setOnSubmit] = useState(false);

    const onFinish = values => {
        setOnSubmit(true);
        api.call('post', '/api/department', res => {
            setOnSubmit(false);
            if (res.status === 'success') {
                message.success('Create department '+res.data.name+' success.');
                history.push('/department');
            }
        }, {
            data: values.department
        })
    }

    return (
        <PageHeader
            title={"Department create"}
        >
            <DepartmentForm onFinish={onFinish} loading={onSubmit} />
        </PageHeader>
    );
}
