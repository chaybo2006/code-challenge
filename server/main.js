import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import {Roles} from 'meteor/alanning:roles';
import {faker} from 'meteor/practicalmeteor:faker';
import './api/Users';
import './api/Department';
import './api/Team';

const SEED_USERNAME = 'admin';
const SEED_PASSWORD = '123456';

if (Meteor.isServer) {
    Meteor.startup(() => {
        if (!Accounts.findUserByUsername(SEED_USERNAME)) {
            let userId = Accounts.createUser({
                username: SEED_USERNAME,
                password: SEED_PASSWORD,
            });
            if (Meteor.roleAssignment.find({ 'user._id': userId }).count() === 0) {
                Roles.createRole('admin', {unlessExists: true});
                Roles.addUsersToRoles(userId, 'admin');
                Meteor.users.update({_id: userId}, {
                    $set: {roles: Roles.getRolesForUser(userId)}
                });
            }
        }

        if (Meteor.roles.find({'_id': 'manager'}).count() === 0) {
            Roles.createRole('manager', {unlessExists: true});
        }

        if (Meteor.roles.find({'_id': 'staff'}).count() === 0) {
            Roles.createRole('staff', {unlessExists: true});
        }

        if (Roles.getUsersInRole('manager').count() === 0) {
            let userId = Accounts.createUser({
                username: faker.internet.userName(),
                password: faker.internet.password()
            });
            Roles.addUsersToRoles(userId, 'manager');
            Meteor.users.update({_id: userId}, {
                $set: {
                    roles: Roles.getRolesForUser(userId),
                    profile: {
                        fullName: faker.name.findName(),
                        email: faker.internet.email(),
                        birthday: faker.date.past(),
                        position: 'manager'
                    }
                }
            });
        }
        if (Meteor.users.find({roles: 'staff'}).count() < 1500) {
            for (let i = 0; i <= 1500; i++) {
                let userId = null;
                try {
                    userId = Accounts.createUser({
                        username: faker.internet.userName(),
                        password: faker.internet.password(),
                    });
                }catch (e) {
                    userId = null;
                }
                if (userId) {
                    Roles.addUsersToRoles(userId, 'staff');
                    Meteor.users.update({_id: userId}, {
                        $set: {
                            roles: Roles.getRolesForUser(userId),
                            profile: {
                                fullName: faker.name.findName(),
                                email: faker.internet.email(),
                                birthday: faker.date.past(),
                                position: 'staff'
                            }
                        }
                    });
                }
            }
        }

    });
}
