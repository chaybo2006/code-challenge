import {Meteor} from "meteor/meteor";
import {Restivus} from 'meteor/mrest:restivus';
import {Roles} from 'meteor/alanning:roles';

var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
});

Api.addRoute('check-user', {authRequired: true}, {
    get: {
        action: function() {
            return this.user;
        }
    }
});

Api.addCollection(Meteor.users, {
    routeOptions: {
        authRequired: true,
        roleRequired: "admin"
    },
    endpoints: {
        getAll: {
            action: function () {
                let q = this.queryParams;
                let query = {};
                let more = {};
                if (q.roles)
                    query.roles = q.roles;
                if (q.skip && q.limit) {
                    more.skip = (q.skip - 1) * q.limit;
                    more.limit = parseInt(q.limit);
                }
                return {
                    status: 'success',
                    count: Meteor.users.find(query).count(),
                    data: Meteor.users.find(query, {
                        ...more,
                        fields: {
                            roles: 1,
                            profile: 1,
                            username: 1
                        },
                        sort: {createdAt: -1}
                    }).fetch()
                };
            }
        },
        get: {
            action: function () {
                let id = this.urlParams.id;
                let result = Meteor.users.findOne({_id: id}, {
                    fields: {
                        roles: 1,
                        profile: 1,
                        username: 1
                    }
                });
                return {
                    status: 'success',
                    data: result
                };
            }
        },
        put: {
            action: function () {
                let id = this.urlParams.id;
                let body = this.bodyParams;
                let update = Meteor.users.update(id, {
                    $set: {
                        profile: body.profile
                    }
                });
                if (update) {
                    if (body.user.password)
                        Accounts.setPassword(id, body.user.password);
                    if (body.user.username)
                        Accounts.setUsername(id, body.user.username);
                    if (!Roles.userIsInRole(id, 'admin')) {
                        Roles.setUserRoles(id, body.profile.position);
                        Meteor.users.update({_id: id}, {
                            $set: {roles: Roles.getRolesForUser(id)}
                        });
                    }
                    return {
                        status: 'success',
                        data: Meteor.users.findOne(id)
                    }
                }
                return {
                    statusCode: 500,
                    body: 'Failed to update user '+body.profile.fullName
                }
            }
        },
        post: {
            action: function () {
                let body = this.bodyParams;
                let userId = Accounts.createUser({
                    username: body.user.username,
                    password: body.user.password,
                });
                let update = Meteor.users.update(userId, {
                    $set: {
                        profile: body.profile
                    }
                });
                if (update) {
                    Roles.addUsersToRoles(userId, body.profile.position);
                    Meteor.users.update({_id: userId}, {
                        $set: {roles: Roles.getRolesForUser(userId)}
                    });

                    return {
                        status: 'success',
                        data: Meteor.users.findOne(userId)
                    }
                }

                return {
                    statusCode: 500,
                    body: 'Failed to create user.'
                }
            }
        }
    }
});
