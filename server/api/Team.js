import {Mongo} from "meteor/mongo";
import {Restivus} from 'meteor/mrest:restivus';
import {Meteor} from "meteor/meteor";
import {DepartmentCollection} from "./Department";

const TeamCollection = new Mongo.Collection('team');

TeamCollection.before.insert((id, doc) => {
    doc.createdAt = new Date();
});

TeamCollection.before.update((id, doc, fieldNames, modifier) => {
    modifier.createdAt = doc.createdAt;
});

var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
});

/*Api.addRoute('user-not-in-team/:id', {authRequired: true}, {
    get: {
        action: function () {
            let team = TeamCollection.findOne(this.urlParams.id);
            return {
                status: 'success',
                data: Meteor.users.find({_id: {$nin: team.staffs || []}, roles: 'staff'}, {
                    fields: {
                        username: 1,
                        profile: 1,
                        roles: 1
                    }
                }).fetch()
            };
        }
    }
});*/

Api.addCollection(TeamCollection, {
    routeOptions: {
        authRequired: true,
        roleRequired: ["admin", "manager"]
    },
    endpoints: {
        getAll: {
            action: function () {
                let data = TeamCollection.find({}, {
                    sort: {createdAt: -1}
                }).map(team => {
                    team.department = DepartmentCollection.findOne(team.department, {
                        fields: {
                            name: 1
                        }
                    });
                    return team;
                });
                return {
                    status: 'success',
                    data: data
                };
            }
        }
    }
});
