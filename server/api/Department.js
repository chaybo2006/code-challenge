import {Mongo} from "meteor/mongo";
import {Restivus} from 'meteor/mrest:restivus';
import {Meteor} from "meteor/meteor";

export const DepartmentCollection = new Mongo.Collection('department');

DepartmentCollection.before.insert((id, doc) => {
    doc.createdAt = new Date();
});

DepartmentCollection.before.update((id, doc, fieldNames, modifier, options) => {
    modifier.createdAt = doc.createdAt;
});

var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
});

Api.addCollection(DepartmentCollection, {
    routeOptions: {
        authRequired: true,
        roleRequired: ["admin", "manager"]
    },
    endpoints: {
        getAll: {
            action: function () {
                let data = DepartmentCollection.find({}, {
                    sort: {createdAt: -1}
                }).map(depart => {
                    depart.belongto = Meteor.users.findOne(depart.belongto, {
                        fields: {
                            roles: 1,
                            profile: 1,
                            username: 1
                        }
                    });
                    return depart;
                });
                return {
                    status: 'success',
                    data: data
                };
            }
        }
    }
});
