#Code Challenge

## Installation
Download meteor on [Meteor](https://www.meteor.com/developers/install)
```bash
meteor npm install
```

##Run on local

```bash
meteor run
```
When run please wait for app render Staff.
```
Account: admin
Password: 123456
```

##To run test app

```bash
meteor test --driver-package meteortesting:mocha --full-app
```

##Build to server

```bash
meteor build <output path>
```

Expand file demo-code-challege.tar.gz

##README BUILD
This is a Meteor application bundle. It has only one external dependency:
Node.js v12.20.1. To run the application:
```bash
(cd programs/server && npm install) &&
export MONGO_URL='mongodb://user:password@host:port/databasename' &&
export ROOT_URL='http://example.com' &&
export MAIL_URL='smtp://user:password@mailhost:port/' &&
node main.js
```

Use the PORT environment variable to set the port where the
application will listen. The default is 80, but that will require
root on most systems.

Find out more about Meteor at meteor.com.
